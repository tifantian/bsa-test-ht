export class FighterState {
  constructor({ health, attack, defense, _id }, position, criticalCombination) {
    this.id = _id;
    this.position = position;
    this.health = health;
    this.totalHealth = health;
    this.attack = attack;
    this.defense = defense;
    this.onBlockNow = false;
    this.onAttackNow = false;
    this.criticalCombination = criticalCombination;
    this.pressedCriticalKeys = [];
    this.canUseCriticalCombination = true;
  };

  changeHealth(newHealth) {
    this.health = newHealth;
  };

  setAttackNow(value) {
    this.onAttackNow = value;
  };

  setBlockNow(value) {
    this.onBlockNow = value;
  };

  getHealthPercentage() {
    if(this.health < 0) return 0;
    return (this.health / this.totalHealth) * 100;
  }

  onKeyDownCriticalKey(key) {
    if(!this.canUseCriticalCombination) return;
    if(this.pressedCriticalKeys.includes(key)) return;
    this.pressedCriticalKeys = [...this.pressedCriticalKeys, key];
  }

  onKeyUpCriticalKey(key) {
    if(!this.pressedCriticalKeys.includes(key)) return;
    this.pressedCriticalKeys = this.pressedCriticalKeys.filter(item => item !== key);
  }

  isCriticalHit() {
    return this.pressedCriticalKeys.length === this.criticalCombination.length;
  }

  clearPressedKeys() {
    this.pressedCriticalKeys = [];
  }

  setTenSecondsTimeout() {
    this.canUseCriticalCombination = false;
    const unblockCriticalCombination = () => this.canUseCriticalCombination = true;
    const timer = setTimeout(unblockCriticalCombination, 10 * 1000);
  }

};

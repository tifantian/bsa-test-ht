import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { showWinnerModal } from './modal/winner';
import { fight } from './fight';

export function renderArena(selectedFighters) {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  const closeArena = () => root.removeChild(arena);

  const winner = fight(...selectedFighters).then(winner => {
    showWinnerModal(winner, closeArena);
  });

}

function createArena(selectedFighters) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);

  arena.append(healthIndicators, fighters);
  return arena;
}

function getHealthState(percentage) {
  if (percentage > 50) return 'high';
  if (percentage < 15) return 'low';
  return 'medium';
}

export function updateHealthIndicator(id, position, percentage) {
  const healthIndicator = document.getElementById(`${position}-fighter-indicator`);
  const healthStates = ['high', 'medium', 'low'];

  healthIndicator.style.width = `${percentage}%`;
  healthIndicator.classList.remove(...healthStates);
  const currentClass = getHealthState(percentage);
  healthIndicator.classList.add(currentClass);
};

function createHealthIndicators(leftFighter, rightFighter) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter, position) {
  const { name, _id } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: `arena___health-bar high`, attributes: { id: `${position}-fighter-indicator` } });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter, secondFighter) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter, position) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}

export function getFightersUpdateFunction(fighter) {
  const leftFighter = document.querySelectorAll(`.arena___left-fighter`)[0];
  const rightFighter = document.querySelectorAll(`.arena___right-fighter`)[0];
  return fighter => {
    const fighterElement = fighter.position === 'left' ? leftFighter : rightFighter;
    let borderStyles = 'none';
    const borderPosition = {
      left: 'borderRight',
      right: 'borderLeft'
    };
    if (fighter.onBlockNow) borderStyles = '20px solid red';
    if (fighter.onAttackNow) borderStyles = '20px solid green';
    if (fighter.isCriticalHit()) borderStyles = '40px solid black';
    fighterElement.style[borderPosition[fighter.position]] = borderStyles;
  };
}

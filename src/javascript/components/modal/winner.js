import { showModal } from './modal';
import { createFighterPreview } from '../fighterPreview';
import App from '../../app'; 

export function showWinnerModal(fighter, closeArena) {
  const title = `The Winner is ${fighter.name}`;
  const bodyElement = createFighterPreview(fighter, 'right');
  const onClose = () => (closeArena(), new App());
  showModal({ title, bodyElement, onClose });
}

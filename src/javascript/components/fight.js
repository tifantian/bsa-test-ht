import { controls } from '../../constants/controls';
import { getRand } from '../helpers/codeHelper';
import { updateHealthIndicator, getFightersUpdateFunction } from './arena';
import { FighterState } from './FighterState';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const first = new FighterState(firstFighter, 'left', controls.PlayerOneCriticalHitCombination);
    const second = new FighterState(secondFighter, 'right', controls.PlayerTwoCriticalHitCombination);

    const checkEndOfGame = () => {
      const loser = [first, second].find(fighter => fighter.health <= 0);
      if (!loser) return;
      const winner = loser.id === first.id ? secondFighter : firstFighter;
      removeEventListeners();
      resolve(winner);
    };

    const updateFighterView = getFightersUpdateFunction();

    const changeDefenderHealth = (defender, damage) => {
      defender.changeHealth(defender.health - damage);
      updateHealthIndicator(defender.id, defender.position, defender.getHealthPercentage());
    }

    const createAttackAction = (attacker, defender) => {
      if (attacker.onAttackNow || attacker.onBlockNow) return;
      attacker.setAttackNow(true);
      const damage = getDamage(attacker, defender);
      changeDefenderHealth(defender, damage);
    };

    const createCriticalAttackAction = (attacker, defender) => {
      if (attacker.onBlockNow) return;
      const damage = getCriticalDamage(attacker, defender);
      changeDefenderHealth(defender, damage);
      attacker.clearPressedKeys();
      attacker.setTenSecondsTimeout();
    };

    const checkCriticalCombination = (attacker, defender, event) => {
      if(attacker.criticalCombination.includes(event.code)) {
        attacker.onKeyDownCriticalKey(event.code);
        if(attacker.isCriticalHit() && !attacker.onBlockNow) {
          updateFighterView(attacker);
          createCriticalAttackAction(attacker, defender);
        }
      }
    };

    const keyUpCriticalCombination = (fighter, event) => {
      if(fighter.criticalCombination.includes(event.code)) {
        fighter.onKeyUpCriticalKey(event.code);
        updateFighterView(fighter);
      };
    }

    const keyDownEventListener = event => {
      switch (event.code) {
        case controls.PlayerOneAttack:
          createAttackAction(first, second);
          updateFighterView(first);
          break;
        case controls.PlayerTwoAttack:
          createAttackAction(second, first);
          updateFighterView(second);
          break;
        case controls.PlayerOneBlock:
          if (!first.onBlockNow) first.setBlockNow(true);
          updateFighterView(first);
          break;
        case controls.PlayerTwoBlock:
          if (!second.onBlockNow) second.setBlockNow(true);
          updateFighterView(second);
          break;
      };
      checkCriticalCombination(first, second, event);
      checkCriticalCombination(second, first, event);
      checkEndOfGame();
    };

    const keyUpEventListener = event => {
      switch (event.code) {
        case controls.PlayerOneAttack:
          first.setAttackNow(false);
          updateFighterView(first);
          break;
        case controls.PlayerTwoAttack:
          second.setAttackNow(false);
          updateFighterView(second);
          break;
        case controls.PlayerOneBlock:
          first.setBlockNow(false);
          updateFighterView(first);
          break;
        case controls.PlayerTwoBlock:
          second.setBlockNow(false);
          updateFighterView(second);
          break;
      }
      keyUpCriticalCombination(first, event);
      keyUpCriticalCombination(second, event);
      checkEndOfGame();
    };

    window.addEventListener('keydown', keyDownEventListener, false);
    window.addEventListener('keyup', keyUpEventListener, false);

    function removeEventListeners() {
      window.removeEventListener('keydown', keyDownEventListener, false);
      window.removeEventListener('keyup', keyUpEventListener, false);
    }
  });
}

// attacker, defender - FighterState
export function getDamage(attacker, defender) {
  const { onBlockNow } = defender;
  if(onBlockNow) return 0;
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? Number(damage.toFixed(2)) : 0;
}

export function getCriticalDamage(attacker, defender) {
  const damage = 2 * getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  return fighter.attack * getChance();
}

export function getBlockPower(fighter) {
  return fighter.defense * getChance();
}

export function getChance() {
  const MINIMUM_CHANCE = 1;
  const MAXIMUM_CHANCE = 2;
  const DECIMAL = 2;
  return getRand(MINIMUM_CHANCE, MAXIMUM_CHANCE, DECIMAL);
}

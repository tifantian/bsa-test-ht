import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if(fighter) {
    const fighterImage = createFighterImage(fighter);
    const fighterDetails = createFighterDetailsBlock(fighter);
    fighterElement.append(fighterImage, fighterDetails);
  }

  return fighterElement;
}

export function createFighterDetailsBlock(fighter) {
  const detailsBlock = createElement({
    tagName: 'div',
    className: `fighter-preview___details`
  });
  const { _id, source, ...details } = fighter;
  const fighterProperties = Object.keys(details).map(key => createFighterProperty(key, fighter[key]));
  detailsBlock.append(...fighterProperties);
  return detailsBlock;
}

export function createFighterProperty(label, value) {
  const fighterProperty = createElement({
    tagName: 'div',
    className: `fighter-preview___property ${label}`
  });

  const addLabel = !['name'].includes(label);

  const elements = [addLabel && label, value].filter(Boolean).map(item => {
    const span = createElement({ tagName: 'span' });
    span.innerText = item;
    return span;
  });
  fighterProperty.append(...elements);
  return fighterProperty;
}

export function createFighterImage(fighter) {
  if (!fighter) return;
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    title: name,
    alt: name,
    attributes,
  });

  return imgElement;
}
